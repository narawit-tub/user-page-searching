import { Input, Col, Row, AutoComplete, Divider, Button, Tooltip } from "antd";
import axios from "axios";
import { useState } from "react";
import { ReactComponent as HumanLogo } from "../assets/icons/human-resources.svg";
import UserProfile from "../components/UserProfile";
import { SearchOutlined } from "@ant-design/icons";

const divStyle = {
  width: "40%",
  maxWidth: "500px",
  backgroundColor: "#ffffff",
  height: "70%",
  borderRadius: "5px",
};

const SearchUserForm = () => {
  const [userFounded, setUserFounded] = useState([]); //usd in option
  const [resultCached, setResultCached] = useState([]);
  const [userSelected, setUserSelected] = useState();

  const onSearch = async (keyword) => {
    // https://jsonplaceholder.typicode.com/users
    if (keyword.trim().length === 0) {
      setUserFounded([]);
      setResultCached([]);
      return;
    }

    let result;
    if (resultCached.length === 0) {
      result = await axios
        .get("https://jsonplaceholder.typicode.com/users")
        .then(function (response) {
          // handle success
          return response.data;
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function (result) {
          // always executed
          return result;
        });
    } else {
      result = resultCached;
    }

    setResultCached(result);
    findPeople(keyword, result);
  };

  const onSelect = (name) => {
    // const user = resultCached.filter((user) => user.name === name)[0];
    const selectedUser = resultCached.filter(
      (user) => user.name.toLowerCase() === name
    )[0];
    setUserSelected(selectedUser);
  };

  const clearUserSelect = () => {
    setUserSelected();
    setUserFounded([]);
  };

  const findPeople = (keyword, users) => {
    let userFound = [];

    users.forEach((user) => {
      let name = user.name.toLowerCase();
      let re = new RegExp(`${keyword.toLowerCase()}`);

      if (name.search(re) > -1) {
        userFound.push({
          value: name,
          data: user,
        });
      }
    });

    setUserFounded(userFound);
  };

  return (
    <div style={{ ...divStyle }}>
      <Row style={{ height: "60%" }} justify="center" align="middle">
        <Col
          span={20}
          style={{
            width: "100%",
            height: "80%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          {userSelected ? (
            <UserProfile
              name={userSelected.name}
              email={userSelected.email}
              phone={userSelected.phone}
              website={userSelected.website}
              company={userSelected.company.name}
            />
          ) : (
            <HumanLogo
              style={{
                height: "100%",
                margin: "5%",
                fill: userFounded.length > 0 ? "#b8b5ff" : "#edeef7",
              }}
            />
          )}
        </Col>
      </Row>
      <Divider style={{ margin: 0 }}>
        {userSelected
          ? "Or you can search another one"
          : userFounded.length === 0
          ? "Start searching!"
          : `Found ${userFounded.length} users`}
      </Divider>
      <Row justify="center" align="middle" style={{ height: "30%" }}>
        {userSelected ? (
          <Col
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button icon={<SearchOutlined />} onClick={() => clearUserSelect()}>
              Search
            </Button>
          </Col>
        ) : (
          <Col span={18}>
            <Input.Group compact>
              <AutoComplete
                style={{ width: "100%" }}
                placeholder="Type the name who want to search"
                options={userFounded}
                onChange={onSearch}
                onSelect={onSelect}
              />
            </Input.Group>
          </Col>
        )}
      </Row>
    </div>
  );
};

export default SearchUserForm;
