import { Card } from "antd";
import {
  MailOutlined,
  PhoneOutlined,
  GlobalOutlined,
  IdcardOutlined,
} from "@ant-design/icons";

const UserProfile = ({ name, email, phone, website, company }) => {
  return (
    <div
      style={{
        // height: "100%",
        width: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Card size="small" title={name} style={{ width: "90%" }}>
        <p>
          <MailOutlined /> {email}
        </p>
        <p>
          <PhoneOutlined /> {phone}
        </p>
        <p>
          <GlobalOutlined /> {website}
        </p>
        <p>
          <IdcardOutlined /> {company}
        </p>
      </Card>
    </div>
  );
};

export default UserProfile;
