import "./App.css";
import SearchForm from "./components/SearchForm";

function App() {
  const divStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    backgroundColor: "#edeef7",
  };

  return (
    <div style={{ ...divStyle }}>
      <SearchForm></SearchForm>
    </div>
  );
}

export default App;
